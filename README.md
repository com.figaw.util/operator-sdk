# Operator SDK in Docker

> NB: If you use windows and build in WSL,
> make sure the scripts have LF line endings.

## Build

    ./build.sh

## Run Interactively

    docker run --rm -it \
        -w /go/src \
        -v operator-sdk:/go/pkg \
        -v $(pwd):/go/src \
        -v //var/run/docker.sock:/var/run/docker.sock \
        --privileged \
        figaw/operator-sdk

> `-v operator-sdk:/go/pkg` reuses the dependencies,
> if you're going to run it a lot.
> For Docker:
>
>     -v //var/run/docker.sock:/var/run/docker.sock \
>     --privileged \

## Test

    ./test.sh
